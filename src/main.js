import Vue from 'vue'
import App from './App.vue'
// eslint-disable-next-line no-unused-vars
import vuetify from '@/plugins/vuetify' // path to vuetify export
import {Message} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false
Vue.prototype.debug = true
Vue.prototype.$message = Message

new Vue({
  render: h => h(App),
  vuetify
}).$mount('#app')
