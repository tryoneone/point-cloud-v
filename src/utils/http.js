// 封装请求
import axios from 'axios'

// 请求
const http = axios.create({
  baseURL: 'http://localhost:5000',
  timeout: 1000000,
})

// 请求拦截
http.interceptors.request.use(config => {
  //请求头设置
  config.headers = {'Content-Type': "application/json;charset=utf-8"}
  return config
}, err => {
  console.log(err);
})

// 响应拦截
http.interceptors.response.use(arr => {
  console.log(arr)
  // 对响应码的处理
  switch (arr.status) {
    case 200:
      // Vue.$message.success(arr.statusText)
      console.log('success: ' + arr.statusText)
      break
    case 201:
      // Vue.$message.success(arr.statusText)
      console.log('success: ' + arr.statusText)
      break
    case 204:
      // Vue.$message.success(arr.statusText)
      console.log('success: ' + arr.statusText)
      break
    case 400:
      // Vue.$message.warning(arr.statusText)
      console.log('warning: ' + arr.statusText)
      break
    case 401:
      // Vue.$message.warning(arr.statusText)
      console.log('warning: ' + arr.statusText)
      break
    case 403:
      // Vue.$message.warning(arr.statusText)
      console.log('warning: ' + arr.statusText)
      break
    case 404:
      // Vue.$message.warning(arr.statusText)
      console.log('warning: ' + arr.statusText)
      break
    case 422:
      // Vue.$message.warning(arr.statusText)
      console.log('warning: ' + arr.statusText)
      break
    case 500:
      // Vue.$message.error(arr.statusText)
      console.log('error: ' + arr.statusText)
      break
  }
  return arr.data
}, err => {
  // Vue.$message.error(err)
  console.log(err)
})

// 导出
export default http
