// 封装请求参数
import http from '../utils/http'

function request({ method = "get", url, data = {}, params = {} }) {
  return http({
    method,
    url,
    data,
    params,
  })
}

export default request
