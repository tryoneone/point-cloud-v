// Index页面请求
import request from '@/api/request'

export const uploadFile = (data) => request({
  method: 'post',
  url: '/uploadFile',
  data: data
})
